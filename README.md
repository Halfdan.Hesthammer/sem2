# Mitt sjakk program

Dette er et sjakkSpill som utnytter java swing og JPanels til å spille.
Sjakkreglene er de samme som vanlige sjakkregler - rokkade.

Det er hvit som begynner, og brikkene kan flytte seg forskellig. Her er chatgpt med en god forklaring:
"Spillet begynner med hvite brikker som tar det første trekket, og deretter veksler spillerne på å ta trekk.
Hver brikke har sin egen måte å flytte på:
Konge: kan flytte ett felt i hvilken som helst retning (vertikalt, horisontalt eller diagonalt).
Dronning: kan flytte hvilket som helst antall felter i rett linje, enten vertikalt, horisontalt eller diagonalt.
Tårn: kan flytte hvilket som helst antall felter vertikalt eller horisontalt.
Løper: kan flytte hvilket som helst antall felter diagonalt.
Ridder: flytter i en L-formet bevegelse, som to felter i en rett linje og deretter ett felt til siden. Riddere kan hoppe over andre brikker.
Bonde: flytter ett felt fremover (mot motstanderens side av brettet) men slår diagonalt. En bonde kan flytte to felter fremover fra sin startposisjon, men bare hvis den ikke er blokkert av andre brikker.
En spiller kan slå en motstanders brikke ved å flytte sin egen brikke til et felt som inneholder motstanderens brikke. Den slåtte brikken fjernes fra brettet.
En spiller er i sjakk når deres konge er truet av en motstanders brikke. Spilleren må deretter gjøre et trekk som fjerner sjakken, enten ved å flytte kongen, blokkere angrepet eller slå den truende brikken.
Sjakk matt oppstår når en konge er i sjakk og det ikke finnes lovlige trekk som kan fjerne sjakken. Den spilleren som setter motstanderens konge i sjakk matt, vinner spillet.
Hvis en spiller ikke kan gjøre et lovlig trekk og ikke er i sjakk, er det patt (remis). Spillet ender da i uavgjort."

Her er link til visuell forklaring av hvordan man spiller.
https://youtu.be/uy86afDCDsA




Mange av testene blir gjort manuelt og visuelt med JPanels og JComponents, der du må forøvrig trykke og sjekke forskjellige punkter og dens



package no.uib.inf101.sem2.grid;

import java.util.ArrayList;
import java.util.Iterator;

public class Grid<E> implements IGrid<E> {

    ArrayList<E> grid;
    int rows;
    int cols;

    /**
     * 
     * @param rows
     * @param cols
     */
    public Grid(int rows, int cols) {
        this.grid = new ArrayList<>();
        this.rows = rows;
        this.cols = cols;
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                this.grid.add(null);
            }
        }

    }

    @Override
    public Iterator<E> iterator() {
        // TODO Auto-generated method stub
        return grid.iterator();
    }

    @Override
    public E get(GridPosition pos) {
        // TODO Auto-generated method stub
        if (onGrid(pos)) {

            return grid.get(posToIndex(pos));
        }
        throw new IndexOutOfBoundsException();
    }

    @Override
    public void set(GridPosition pos, E value) {
        // TODO Auto-generated method stub
        if (onGrid(pos)) {
            grid.set(posToIndex(pos), value);
            return;
        }
        throw new IndexOutOfBoundsException();

    }

    @Override
    public boolean onGrid(GridPosition pos) {
        if (pos == null) {
            return false;
        }

        return (pos.col() >= 0 && pos.col() < cols) && (pos.row() >= 0 && pos.row() < rows);
    }

    @Override
    public int rows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int cols() {
        // TODO Auto-generated method stub
        return cols;
    }

    private int posToIndex(GridPosition pos) {
        return pos.col() + pos.row() * this.cols;
    }

    @Override
    public void fill(E value) {
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                GridPosition pos = new GridPosition(row, col);
                grid.set(posToIndex(pos), value);
            }
        }
    }

    /**
     * 
     * @return all positions in grid
     */
    public ArrayList<GridPosition> getAllGridPositions() {
        ArrayList<GridPosition> positions = new ArrayList<>();
        for (int row = 0; row < rows(); row++) {
            for (int col = 0; col < cols(); col++) {
                positions.add(new GridPosition(row, col));
            }
        }
        return positions;
    }

}

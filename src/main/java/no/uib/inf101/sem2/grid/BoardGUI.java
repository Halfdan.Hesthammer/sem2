package no.uib.inf101.sem2.grid;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import no.uib.inf101.sem2.chess.Chess;
import no.uib.inf101.sem2.chess.ChessBoard;
import no.uib.inf101.sem2.chess.Move;

public class BoardGUI extends JPanel {
    private static int SQUARE_SIZE = 50;
    private Chess game;
    private int rows;
    private int cols;

    public boolean firstSelected = false;
    private GridPosition pos1;
    private Square selectedSquare;

    public BoardGUI(Chess game) {
        this.game = game;
        this.rows = game.getBoard().rows();
        this.cols = game.getBoard().cols();
        this.setFocusable(true);
        this.setPreferredSize(new Dimension(rows * SQUARE_SIZE * 5, cols * SQUARE_SIZE * 5));
        this.setSize(getPreferredSize());
        setLayout(new GridLayout(rows, cols, 1, 1));

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                Square square = new Square(new GridPosition(row, col));
                Color color = (row + col) % 2 == 0 ? Color.WHITE : Color.GRAY;
                square.setBackground(color);
                square.setOriginalColor(color);
                add(square);
            }
        }
    }

    /**
     * 
     * @param square handles the click and calls the move for the game
     */
    private void handleClick(Square square) {
        GridPosition pos = square.pos;
        if (!firstSelected) {
            pos1 = pos;
            firstSelected = true;
            square.setBackground(Color.RED);
            selectedSquare = square;
            showLegalMoves(game.getLegalPositions(pos1));
        } else {

            game.move(new Move(pos1, pos));
            firstSelected = false;
            selectedSquare.setBackground(selectedSquare.originalColor);
            selectedSquare.repaint();
            square.repaint();
            disShowlegalMoves();
        }
    }

    private void showLegalMoves(ArrayList<GridPosition> legalPositions) {

        for (Component c : getComponents()) {
            if (c instanceof Square && legalPositions.contains(((Square) c).pos)) {
                ((Square) c).setBackground(Color.RED);
            }
        }
    }

    private void disShowlegalMoves() {

        for (Component c : getComponents()) {
            if (c instanceof Square) {
                Square square = (Square) c;
                square.setBackground(square.originalColor);

            }
        }
    }

    /**
     * A intern square to hold the position and the color of the
     */
    class Square extends JPanel {
        private GridPosition pos;
        private Color originalColor;

        public Square(GridPosition pos) {
            this.pos = pos;
            this.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    handleClick(Square.this);
                }
            });
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            if (game.getBoard().get(pos) != null) {
                ImageIcon icon = game.getBoard().get(pos).getIcon();
                g.drawImage(icon.getImage(), 0, 0, getWidth(), getHeight(), this);
            }
        }

        /**
         * Sets the original backgroundcolor
         * 
         * @param originalColor
         */
        public void setOriginalColor(Color originalColor) {
            this.originalColor = originalColor;
        }
    }

}

package no.uib.inf101.sem2.grid;

public interface IGrid<E> extends Iterable<E> {
    /**
     * 
     * @param pos to get value from
     * @return
     */
    public E get(GridPosition pos);

    /**
     * 
     * @param pos   to set
     * @param value to set
     */
    public void set(GridPosition pos, E value);

    /**
     * 
     * @return nr of rows
     */
    public int rows();

    /**
     * 
     * @return nr of cols
     */
    public int cols();

    public boolean onGrid(GridPosition pos);

    /**
     * Fills the grid with value
     * 
     * @param value
     */
    public void fill(E value);

}

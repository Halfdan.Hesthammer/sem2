package no.uib.inf101.sem2.grid;

public class GridPosition {
    private int row;
    private int col;

    public GridPosition(int row, int col) {
        this.row = row;
        this.col = col;
    }

    @Override
    public boolean equals(Object obj) {
        // TODO Auto-generated method stub
        if (!(obj instanceof GridPosition)) {
            return false;
        }
        return ((GridPosition) obj).col == this.col && ((GridPosition) obj).row == this.row;
    }

    public int col() {
        return col;
    }

    public int row() {
        return row;
    }
}

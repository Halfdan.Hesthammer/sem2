package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.chess.Chess;
import no.uib.inf101.sem2.chess.IGUI;

public class DummyGUI implements IGUI {

    public DummyGUI(Chess chess) {
    }

    @Override
    public void updateCurrentPlayerLabel() {
        // TODO Auto-generated method stub

    }

    @Override
    public String choosePiece() {
        // TODO Auto-generated method stub
        return "Queen";
    }

    @Override
    public void drawGameOver(String string) {
        // TODO Auto-generated method stub
        System.out.println(string);
    }

    @Override
    public void repaint() {
        // TODO Auto-generated method stub

    }

}

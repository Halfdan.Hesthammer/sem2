package no.uib.inf101.sem2.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import no.uib.inf101.sem2.chess.Chess;
import no.uib.inf101.sem2.chess.ChessBoard;
import no.uib.inf101.sem2.chess.IGUI;
import no.uib.inf101.sem2.chess.pieces.ChessPiece;
import no.uib.inf101.sem2.grid.Grid;
import no.uib.inf101.sem2.grid.BoardGUI;

public class GUI implements IGUI {
    private JFrame frame;
    private BoardGUI panel;
    private Chess game;
    private JLabel currentPlayerLabel;

    public GUI(Chess game) {
        frame = new JFrame();
        this.game = game;
        this.panel = new BoardGUI(game);
        // panel.setSize(frame.getWidth(), frame.getHeight());
        // frame.add(new GridGUI<>(new Grid<>(board.rows(), board.cols())));
        frame.add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(new Dimension(500, 500));
        frame.setBackground(Color.BLUE);

        // Add reset button
        JButton resetButton = new JButton("Reset");
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                game.reset();
                panel.repaint(); // Refresh the panel to show the updated game state
            }
        });

        currentPlayerLabel = new JLabel(game.getCurrentPLayer().getName() + " sin tur");
        currentPlayerLabel.setFont(new Font("Arial", Font.BOLD, 10));
        currentPlayerLabel.setHorizontalAlignment(JLabel.CENTER);
        currentPlayerLabel.setPreferredSize(new Dimension(frame.getWidth(), frame.getHeight() / 13));
        frame.add(currentPlayerLabel, BorderLayout.NORTH);

        frame.add(resetButton, BorderLayout.SOUTH); // Add the button to the frame

        frame.setVisible(true);

    }

    public void drawGameOver(String playerwon) {
        currentPlayerLabel.setText(playerwon + " vant");
    }

    /**
     * from chatgpt
     * Make a option pane for the new Piece
     */
    public String choosePiece() {
        String[] options = { "Queen", "Rook", "Bishop", "Knight" };
        int choice = JOptionPane.showOptionDialog(frame,
                "Choose a piece:",
                "Pawn Promotion",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.PLAIN_MESSAGE,
                null,
                options,
                options[0]);
        return options[choice];
    }

    public void updateCurrentPlayerLabel() {
        currentPlayerLabel.setText(game.getCurrentPLayer().getName() + " sin tur");
    }

    @Override
    public void repaint() {
        // TODO Auto-generated method stub
        panel.repaint();
    }

}

package no.uib.inf101.sem2.player;

import java.awt.Color;

public class Player {

    private Color color;
    private String name;

    public Player(Color color, String name) {
        this.color = color;
        this.name = name;
    }

    public Color getColor() {
        return this.color;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public boolean equals(Object obj) {
        // TODO Auto-generated method stub
        if (!(obj instanceof Player)) {
            return false;
        }
        return ((Player) obj).getColor().equals(this.color) && ((Player) obj).getName().equals(this.name);
    }

}

package no.uib.inf101.sem2;

import no.uib.inf101.sem2.chess.Chess;
import no.uib.inf101.sem2.chess.ChessBoard;
import no.uib.inf101.sem2.grid.BoardGUI;
import no.uib.inf101.sem2.player.Player;
import no.uib.inf101.sem2.view.GUI;
import no.uib.inf101.sem2.view.SampleView;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Main {
  public static void main(String[] args) {
    // SampleView view = new SampleView();

    // JFrame frame = new JFrame();
    // frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    // frame.setTitle("INF101");
    // frame.setContentPane(view);
    // frame.pack();
    // frame.setVisible(true);
    String p1 = JOptionPane.showInputDialog(null, "Enter Player1 name:");
    String p2 = JOptionPane.showInputDialog(null, "Enter Player2 name:");
    Player player1 = new Player(Color.WHITE, p1);
    Player player2 = new Player(Color.BLACK, p2);
    int aiOption = JOptionPane.showConfirmDialog(null, "Play AI?",
        "Choose AI or not AI", JOptionPane.YES_NO_OPTION);

    boolean ai = aiOption == JOptionPane.YES_OPTION;

    Chess game = new Chess(player1, player2, true, ai);
    // Inputer in = new GUIg();
    // ChessGame game = new ChessGame(player1, player2, in);
    // game.run();

    // System.out.println(board.printBoard());
  }
}

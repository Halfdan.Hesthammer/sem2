package no.uib.inf101.sem2.chess;

import java.awt.Color;

import no.uib.inf101.sem2.chess.pieces.Bishop;
import no.uib.inf101.sem2.chess.pieces.ChessPiece;
import no.uib.inf101.sem2.chess.pieces.King;
import no.uib.inf101.sem2.chess.pieces.Knight;
import no.uib.inf101.sem2.chess.pieces.Pawn;
import no.uib.inf101.sem2.chess.pieces.Queen;
import no.uib.inf101.sem2.chess.pieces.Rook;
import no.uib.inf101.sem2.grid.Grid;
import no.uib.inf101.sem2.grid.IGrid;
import no.uib.inf101.sem2.grid.GridPosition;

public class ChessBoard extends Grid<ChessPiece> {

    public ChessBoard() {
        super(8, 8);

        initiateBoard();
    }

    /*
     * Makes the board setup as a normal chessBoard
     */
    private void initiateBoard() {
        fill(null);
        // Set up white pieces. Chatgpt made this.
        set(new GridPosition(7, 0), new Rook(Color.WHITE));
        set(new GridPosition(7, 1), new Knight(Color.WHITE));
        set(new GridPosition(7, 2), new Bishop(Color.WHITE));
        set(new GridPosition(7, 3), new Queen(Color.WHITE));
        set(new GridPosition(7, 4), new King(Color.WHITE));
        set(new GridPosition(7, 5), new Bishop(Color.WHITE));
        set(new GridPosition(7, 6), new Knight(Color.WHITE));
        set(new GridPosition(7, 7), new Rook(Color.WHITE));
        for (int col = 0; col < 8; col++) {
            set(new GridPosition(6, col), new Pawn(Color.WHITE));
        }

        // Set up black pieces

        set(new GridPosition(0, 0), new Rook(Color.BLACK));
        set(new GridPosition(0, 1), new Knight(Color.BLACK));
        set(new GridPosition(0, 2), new Bishop(Color.BLACK));
        set(new GridPosition(0, 3), new Queen(Color.BLACK));
        set(new GridPosition(0, 4), new King(Color.BLACK));
        set(new GridPosition(0, 5), new Bishop(Color.BLACK));
        set(new GridPosition(0, 6), new Knight(Color.BLACK));
        set(new GridPosition(0, 7), new Rook(Color.BLACK));
        for (int col = 0; col < 8; col++) {
            set(new GridPosition(1, col), new Pawn(Color.BLACK));
        }
        // System.out.println(printBoard());
    }

    /**
     * 
     * @return a string of the baord
     */
    public String board2String() {
        String str = "";
        for (int row = 0; row < rows(); row++) {
            for (int col = 0; col < cols(); col++) {
                GridPosition pos = new GridPosition(row, col);
                if (get(pos) == null) {
                    str += "-";
                } else {

                    char c = get(pos).getClass().toString().charAt(38);
                    str += c;
                }

            }
            str += "\n";
        }

        return str;
    }

    /**
     * 
     * @return a copy of the board.
     */
    public ChessBoard copyBoard() {
        ChessBoard board = new ChessBoard();
        for (int row = 0; row < rows(); row++) {
            for (int col = 0; col < cols(); col++) {
                GridPosition pos = new GridPosition(row, col);
                board.set(pos, get(pos));
            }
        }
        return board;
    }

}

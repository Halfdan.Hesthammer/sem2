package no.uib.inf101.sem2.chess.pieces;

import java.awt.Color;

import javax.swing.ImageIcon;

import no.uib.inf101.sem2.chess.ChessBoard;
import no.uib.inf101.sem2.chess.Move;
import no.uib.inf101.sem2.grid.GridPosition;

public class Pawn extends ChessPiece {

    private boolean hasMoved;

    public Pawn(Color color) {
        super(color);
        if (color.equals(Color.WHITE)) {
            this.icon = new ImageIcon("src/main/resources/white_pawn.png");
        } else {
            this.icon = new ImageIcon("src/main/resources/black_pawn.png");
        }
        hasMoved = false;
    }

    @Override
    public boolean isValidMove(Move move, ChessBoard board) {

        if (isSamepPos(move)) {
            return false;
        }
        if (takesOwnPiece(move.to(), board)) {
            return false;
        }
        // Can only move forward, which is "down" (in sense of row number) for white and
        // up for black
        int direction = getColor() == Color.WHITE ? -1 : 1;
        int rowDiff = move.to().row() - move.from().row();
        int colDiff = Math.abs(move.to().col() - move.from().col());

        if (rowDiff != direction && !(rowDiff == 2 * direction && !hasMoved && colDiff == 0)) {
            return false;
        }

        if (colDiff == 0) {
            if (takesEnemyPiece(move.to(), board)) {
                return false;
            }
        } else if (colDiff == 1 && rowDiff == direction) {
            if (!takesEnemyPiece(move.to(), board)) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    /**
     * Sets that the pawn has moved
     */
    public void setMoved() {
        hasMoved = true;
    }

    /**
     * 
     * @return if the pawn has moved before
     */
    public boolean hasMoved() {
        return hasMoved;
    }

    /**
     * 
     * @param move
     * @return if the pawn has gotten to the last row meaning it can promote
     */
    public boolean newPiece(GridPosition move) {
        if (getColor() == Color.WHITE) {
            return move.row() == 0;
        } else {
            return move.row() == 7;
        }
    }

}

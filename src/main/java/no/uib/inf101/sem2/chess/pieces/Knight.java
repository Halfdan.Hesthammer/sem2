package no.uib.inf101.sem2.chess.pieces;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.text.Position;

import no.uib.inf101.sem2.chess.ChessBoard;
import no.uib.inf101.sem2.chess.Move;

public class Knight extends ChessPiece {

    public Knight(Color color) {
        super(color);
        // TODO Auto-generated constructor stub
        if (color.equals(Color.WHITE)) {
            this.icon = new ImageIcon("src/main/resources/white_knight.png");
        } else {
            this.icon = new ImageIcon("src/main/resources/black_knight.png");
        }

    }

    @Override
    public boolean isValidMove(Move move, ChessBoard board) {
        // TODO Auto-generated method stub
        if (isSamepPos(move)) {
            return false;
        }
        if (takesOwnPiece(move.to(), board)) {
            return false;
        }

        // Can move in L shape
        if (Math.abs(move.to().col() - move.from().col()) == 2) {
            if (Math.abs(move.to().row() - move.from().row()) == 1) {
                return true;
            }
        }
        if (Math.abs(move.to().row() - move.from().row()) == 2) {
            if (Math.abs(move.to().col() - move.from().col()) == 1) {
                return true;
            }
        }
        return false;
    }

}

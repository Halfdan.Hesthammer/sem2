package no.uib.inf101.sem2.chess;

import no.uib.inf101.sem2.grid.GridPosition;

public class Move {

    private GridPosition from;
    private GridPosition to;

    public Move(GridPosition from, GridPosition to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public boolean equals(Object obj) {
        // TODO Auto-generated method stub
        if (!(obj instanceof Move)) {
            return false;
        }

        return ((Move) obj).from.equals(this.from) && ((Move) obj).to.equals(this.to);

    }

    public GridPosition from() {
        return from;
    }

    public GridPosition to() {
        return to;
    }

}

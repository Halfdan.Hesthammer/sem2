package no.uib.inf101.sem2.chess.pieces;

import javax.swing.ImageIcon;
import javax.swing.text.Position;

import no.uib.inf101.sem2.chess.ChessBoard;
import no.uib.inf101.sem2.chess.Move;
import no.uib.inf101.sem2.grid.GridPosition;

import java.awt.Color;

public abstract class ChessPiece {
    private Color color;

    ImageIcon icon;

    /**
     * Abstract piece for chessPieces
     * 
     * @param color
     */
    public ChessPiece(Color color) {
        this.color = color;

    }

    /**
     * 
     * @return piece color
     */
    public Color getColor() {
        return this.color;
    }

    /**
     * 
     * @return image icon to the piece
     */
    public ImageIcon getIcon() {
        return this.icon;

    }

    /**
     * Checks if you take your own piece.
     * 
     * @param pos
     * @return
     */
    public boolean takesOwnPiece(GridPosition pos, ChessBoard board) {
        ChessPiece piece = board.get(pos);
        if (piece != null) {
            if (piece.color.equals(this.color)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if it takes an enemy piece.
     * 
     * @param pos
     * @return
     */
    public boolean takesEnemyPiece(GridPosition pos, ChessBoard board) {
        ChessPiece piece = board.get(pos);
        if (piece != null) {
            if (!piece.color.equals(this.color)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 
     * @param move  checks if the move is valid
     * @param board
     * @return
     */
    public abstract boolean isValidMove(Move move, ChessBoard board);

    /**
     * Checks if a piece moved thhrough another piece.
     * 
     * @param move
     * @return
     */
    public boolean movedThroughPiece(Move move, ChessBoard board) {
        GridPosition start = move.from();
        GridPosition end = move.to();
        int rowDiff = end.row() - start.row();
        int colDiff = end.col() - start.col();

        int rowStep = rowDiff != 0 ? rowDiff / Math.abs(rowDiff) : 0;
        int colStep = colDiff != 0 ? colDiff / Math.abs(colDiff) : 0;

        GridPosition currentPosition = new GridPosition(start.row() + rowStep, start.col() + colStep);

        while (!currentPosition.equals(end) && board.onGrid(currentPosition)) {
            if (board.get(currentPosition) != null) {
                return true;
            }
            currentPosition = new GridPosition(currentPosition.row() + rowStep, currentPosition.col() + colStep);
        }

        return false;
    }

    /**
     * Checks if it is the same position
     * 
     * @param move
     * @return
     */
    public boolean isSamepPos(Move move) {
        if (move.to().equals(move.from())) {
            return true;
        }
        return false;
    }

}

package no.uib.inf101.sem2.chess;

import java.util.ArrayList;
import java.util.Random;

import no.uib.inf101.sem2.chess.pieces.Bishop;
import no.uib.inf101.sem2.chess.pieces.ChessPiece;
import no.uib.inf101.sem2.chess.pieces.King;
import no.uib.inf101.sem2.chess.pieces.Knight;
import no.uib.inf101.sem2.chess.pieces.Pawn;
import no.uib.inf101.sem2.chess.pieces.Queen;
import no.uib.inf101.sem2.chess.pieces.Rook;
import no.uib.inf101.sem2.grid.BoardGUI;
import no.uib.inf101.sem2.grid.Grid;
import no.uib.inf101.sem2.grid.GridPosition;
import no.uib.inf101.sem2.player.Player;
import no.uib.inf101.sem2.view.DummyGUI;
import no.uib.inf101.sem2.view.GUI;

public class Chess {

    private ChessBoard board;
    private Player currentPlayer;
    private boolean checkMate;
    private Player player1;
    private Player player2;
    private IGUI gui;
    private boolean ai;

    public Chess(Player player1, Player player2, boolean gui, boolean ai) {
        this.board = new ChessBoard();
        this.ai = ai;
        this.player1 = player1;
        this.player2 = player2;
        currentPlayer = player1;

        if (gui) {
            this.gui = new GUI(this);
        } else {
            this.gui = new DummyGUI(this);
        }

        checkMate = false;
    }

    public ChessBoard getBoard() {
        return board;
    }

    /**
     * Resets the board and game
     */
    public void reset() {
        this.board = new ChessBoard();
        this.currentPlayer = player1;
        this.checkMate = false;
        gui.updateCurrentPlayerLabel();
    }

    /**
     * moves based on a move
     * 
     * @param move
     * @return
     */
    public boolean move(Move move) {
        if (checkMate) {
            return false;
        }
        ArrayList<Move> moves = getLegalMoves(currentPlayer, 1, board);
        if (!board.onGrid(move.from()) && !board.onGrid(move.to())) {
            return false;
        }

        if (moves.contains(move)) {
            board.set(move.to(), board.get(move.from()));
            board.set(move.from(), null);
            if (board.get(move.to()) instanceof Pawn) {
                ((Pawn) board.get(move.to())).setMoved();
                if (((Pawn) board.get(move.to())).newPiece(move.to())) {
                    newPiece(gui.choosePiece(), move.to());
                }

            }

            switchPlayers();
            gui.updateCurrentPlayerLabel();
            if (getLegalMoves(currentPlayer, 1, board).isEmpty()) {
                setCheckMate();
            }

            if (ai) {
                moves = getLegalMoves(currentPlayer, 1, board);
                if (!moves.isEmpty()) {
                    Random rand = new Random();
                    int a = rand.nextInt(moves.size());
                    Move randomMove = moves.get(a);
                    board.set(randomMove.to(), board.get(randomMove.from()));
                    board.set(randomMove.from(), null);
                    switchPlayers();
                    gui.updateCurrentPlayerLabel();
                    gui.repaint();
                    if (getLegalMoves(currentPlayer, 1, board).isEmpty()) {
                        setCheckMate();
                    }
                }
            }

            return true;

        }
        return false;
    }

    private void newPiece(String pieceType, GridPosition position) {
        ChessPiece newPiece = null;
        switch (pieceType) {
            case "Queen":
                newPiece = new Queen(currentPlayer.getColor());
                break;
            case "Rook":
                newPiece = new Rook(currentPlayer.getColor());
                break;
            case "Bishop":
                newPiece = new Bishop(currentPlayer.getColor());
                break;
            case "Knight":
                newPiece = new Knight(currentPlayer.getColor());
                break;
            default:
                break;
        }
        board.set(position, newPiece);
    }

    private void setCheckMate() {
        switchPlayers();
        checkMate = true;
        System.out.println("Checkmate");
        gui.drawGameOver(currentPlayer.getName());
    }

    /**
     * 
     * @return current players name.
     */
    public String getPlayerName() {
        return currentPlayer.getName();
    }

    /**
     * Gets all the legal moves for a player
     * 
     * @param player   the players legal moves
     * @param depth    if the depth is 0 it shouldnt check if he can be checked by
     *                 the move
     * @param curBoard the board to check
     * @return
     */
    public ArrayList<Move> getLegalMoves(Player player, int depth, ChessBoard curBoard) {
        ArrayList<Move> legalMoves = new ArrayList<>();

        for (GridPosition from : curBoard.getAllGridPositions()) {
            for (GridPosition to : curBoard.getAllGridPositions()) {
                Move move = new Move(from, to);
                ChessPiece piece = curBoard.get(from);
                if (piece != null && piece.getColor().equals(player.getColor()) && piece.isValidMove(move, curBoard)
                        && !check(player, move, depth)) {
                    legalMoves.add(move);
                }
            }
        }
        return legalMoves;
    }

    private boolean check(Player player, Move move, int depth) {
        if (depth == 0) {
            return false;
        }

        ChessBoard boardCopy = board.copyBoard();
        boardCopy.set(move.to(), boardCopy.get(move.from()));
        boardCopy.set(move.from(), null);

        Player nextPlayer = player.equals(player1) ? player2 : player1;

        ArrayList<Move> allLegalMoves = getLegalMoves(nextPlayer, depth - 1, boardCopy);
        for (Move move2 : allLegalMoves) {
            if (boardCopy.get(move2.to()) instanceof King
                    && !boardCopy.get(move2.to()).getColor().equals(nextPlayer.getColor())) {
                return true; // The king can be taken only at the specified depth
            }
        }

        return false;
    }

    private void switchPlayers() {
        if (currentPlayer.equals(player1)) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }

    public Player getCurrentPLayer() {
        return currentPlayer;
    }

    /**
     * For testing
     * 
     * @return
     */
    public boolean isMate() {
        return checkMate;
    }

    public ArrayList<GridPosition> getLegalPositions(GridPosition pos1) {
        ArrayList<Move> legalMoves = getLegalMoves(currentPlayer, 1, board);

        ArrayList<GridPosition> positions = new ArrayList<>();
        for (Move m : legalMoves) {
            if (m.from().equals(pos1)) {
                positions.add(m.to());
            }
        }

        return positions;
    }
}
package no.uib.inf101.sem2.chess.pieces;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.text.Position;

import no.uib.inf101.sem2.chess.ChessBoard;
import no.uib.inf101.sem2.chess.Move;

public class King extends ChessPiece {

    public King(Color color) {
        super(color);
        // TODO Auto-generated constructor stub
        if (color.equals(Color.WHITE)) {
            this.icon = new ImageIcon("src/main/resources/white_king.png");
        } else {
            this.icon = new ImageIcon("src/main/resources/black_king.png");
        }

    }

    @Override
    public boolean isValidMove(Move move, ChessBoard board) {
        // TODO Auto-generated method stub

        if (isSamepPos(move)) {
            return false;
        }
        if (takesOwnPiece(move.to(), board)) {
            return false;
        }

        if (movedThroughPiece(move, board)) {
            return false;
        }

        if (Math.abs(move.from().row() - move.to().row()) > 1) {
            return false;
        }
        if (Math.abs(move.from().col() - move.to().col()) > 1) {
            return false;
        }

        return true;
    }

}

package no.uib.inf101.sem2.chess.pieces;

import java.awt.Color;

import javax.swing.ImageIcon;

import no.uib.inf101.sem2.chess.ChessBoard;
import no.uib.inf101.sem2.chess.Move;
import no.uib.inf101.sem2.grid.Grid;
import no.uib.inf101.sem2.grid.GridPosition;

public class Rook extends ChessPiece {

    public Rook(Color color) {
        super(color);
        // TODO Auto-generated constructor stub
        if (color.equals(Color.WHITE)) {
            this.icon = new ImageIcon("src/main/resources/white_rook.png");
        } else {
            this.icon = new ImageIcon("src/main/resources/black_rook.png");
        }

    }

    @Override
    public boolean isValidMove(Move move, ChessBoard board) {
        // TODO Auto-generated method stub
        if (isSamepPos(move)) {
            return false;
        }
        if (takesOwnPiece(move.to(), board)) {
            return false;
        }
        if (movedThroughPiece(move, board)) {
            return false;
        }
        // Can only move vertically or horizontally
        if (Math.abs(move.from().col() - move.to().col()) == 0 || Math.abs(move.from().row() - move.to().row()) == 0) {
            return true;
        }
        return false;
    }

}

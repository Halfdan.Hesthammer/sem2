package no.uib.inf101.sem2.chess;

import no.uib.inf101.sem2.grid.GridPosition;

public interface IGUI {

    public void updateCurrentPlayerLabel();

    public String choosePiece();

    public void drawGameOver(String string);

    public void repaint();

}

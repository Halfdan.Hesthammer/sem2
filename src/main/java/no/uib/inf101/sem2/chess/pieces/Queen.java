package no.uib.inf101.sem2.chess.pieces;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.text.Position;

import no.uib.inf101.sem2.chess.ChessBoard;
import no.uib.inf101.sem2.chess.Move;

public class Queen extends ChessPiece {

    public Queen(Color color) {
        super(color);
        // TODO Auto-generated constructor stub
        if (color.equals(Color.WHITE)) {
            this.icon = new ImageIcon("src/main/resources/white_queen.png");
        } else {
            this.icon = new ImageIcon("src/main/resources/black_queen.png");
        }
    }

    @Override
    public boolean isValidMove(Move move, ChessBoard board) {
        if (isSamepPos(move)) {
            return false;
        }

        // TODO Auto-generated method stub
        if (takesOwnPiece(move.to(), board)) {
            return false;
        }
        if (movedThroughPiece(move, board)) {
            return false;
        }
        // Can move horrizontally or vertically
        if (Math.abs(move.from().col() - move.to().col()) == 0 || Math.abs(move.from().row() - move.to().row()) == 0) {
            return true;
        }
        // Can move diagonally
        if (Math.abs(move.from().col() - move.to().col()) == Math.abs(move.from().row() - move.to().row())) {
            return true;
        }
        return false;
    }

}

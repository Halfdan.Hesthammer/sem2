package no.uib.inf101.sem2;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.Color;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.chess.Chess;
import no.uib.inf101.sem2.chess.Move;
import no.uib.inf101.sem2.chess.pieces.King;
import no.uib.inf101.sem2.chess.pieces.Pawn;
import no.uib.inf101.sem2.chess.pieces.Queen;
import no.uib.inf101.sem2.chess.pieces.Rook;
import no.uib.inf101.sem2.grid.GridPosition;
import no.uib.inf101.sem2.player.Player;

public class ChessTest {

    @Test
    public void notEmptyMoves() {
        Chess chess = new Chess(new Player(Color.WHITE, "P1"), new Player(Color.BLACK, "P2"), false, false);

        assertFalse(chess.getLegalMoves(chess.getCurrentPLayer(), 1, chess.getBoard()).isEmpty());

    }

    @Test
    public void player1Start() {
        Player p1 = new Player(Color.WHITE, "P1");
        Chess chess = new Chess(p1, new Player(Color.BLACK, "P2"), false, false);

        assertTrue(chess.getCurrentPLayer().equals(p1));

    }

    @Test
    void containsRightMove() {
        Chess chess = new Chess(new Player(Color.WHITE, "P1"), new Player(Color.BLACK, "P2"), false, false);

        assertFalse(chess.getLegalMoves(chess.getCurrentPLayer(), 1, chess.getBoard())
                .contains(new Move(new GridPosition(1000, 100), new GridPosition(0, 10))));

        assertTrue(chess.getLegalMoves(chess.getCurrentPLayer(), 1, chess.getBoard())
                .contains(new Move(new GridPosition(6, 1), new GridPosition(4, 1))));
    }

    @Test
    void illegalMove() {
        Chess chess = new Chess(new Player(Color.WHITE, "P1"), new Player(Color.BLACK, "P2"), false, false);

        assertFalse(chess.move(new Move(new GridPosition(6, 1), new GridPosition(6, 2))));
        assertFalse(chess.move(new Move(new GridPosition(6, 1), new GridPosition(2, 3))));
    }

    @Test
    void movePiece() {
        Chess chess = new Chess(new Player(Color.WHITE, "P1"), new Player(Color.BLACK, "P2"), false, false);

        assertTrue(chess.move(new Move(new GridPosition(6, 1), new GridPosition(4, 1))));

        assertTrue(chess.getBoard().get(new GridPosition(4, 1)) instanceof Pawn);
        assertTrue(chess.getBoard().get(new GridPosition(6, 1)) == null);
        assertTrue(chess.getCurrentPLayer().equals(new Player(Color.BLACK, "P2")));
    }

}

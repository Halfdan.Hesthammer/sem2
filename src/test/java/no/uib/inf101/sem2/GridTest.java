package no.uib.inf101.sem2;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

import no.uib.inf101.sem2.grid.Grid;
import no.uib.inf101.sem2.grid.IGrid;
import no.uib.inf101.sem2.grid.GridPosition;

public class GridTest {

    private IGrid<Integer> intGrid;
    private IGrid<Character> charGrid;

    @BeforeEach
    public void setUp() {
        // Create new Grid objects for each test method
        intGrid = new Grid<>(3, 3);
        charGrid = new Grid<>(2, 2);
    }

    @Test
    public void testGetAndSetElement() {

        GridPosition intPos = new GridPosition(1, 1);
        intGrid.set(intPos, 42);
        assertEquals(42, intGrid.get(intPos));

        GridPosition charPos = new GridPosition(0, 1);
        charGrid.set(charPos, 'a');
        assertEquals('a', charGrid.get(charPos));
    }

    @Test
    public void testRowsAndCols() {
        assertEquals(3, intGrid.rows());
        assertEquals(3, intGrid.cols());

        assertEquals(2, charGrid.rows());
        assertEquals(2, charGrid.cols());
    }

    @Test
    public void fillTest() {
        charGrid.fill('c');
        for (char c : charGrid) {
            assertTrue(c == 'c');
        }

        intGrid.fill(1);
        for (int i : intGrid) {
            assertTrue(i == 1);
        }
    }

    @Test
    public void testOutOfBounds() {

        assertThrows(IndexOutOfBoundsException.class, () -> intGrid.get(new GridPosition(3, 3)));

        assertThrows(IndexOutOfBoundsException.class, () -> charGrid.get(new GridPosition(-1, 2)));
    }

}

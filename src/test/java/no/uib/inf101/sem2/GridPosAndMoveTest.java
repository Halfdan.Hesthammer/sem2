package no.uib.inf101.sem2;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.chess.Move;
import no.uib.inf101.sem2.grid.GridPosition;

public class GridPosAndMoveTest {

    @Test
    public void testEqaul() {
        GridPosition pos1 = new GridPosition(2, 3);
        GridPosition pos2 = new GridPosition(2, 3);

        GridPosition pos3 = new GridPosition(2, 2);
        assertTrue(pos1.equals(pos2));
        assertFalse(pos1.equals(pos3));

        Move move1 = new Move(pos2, pos3);
        Move move2 = new Move(pos2, pos3);
        Move move3 = new Move(pos2, pos1);

        assertTrue(move1.equals(move2));
        assertFalse(move1.equals(move3));

    }

}
